package com.itszuvalex.itszulib.core;

public enum EnumAutomaticIO {
    NONE,
    INPUT,
    OUTPUT
}

package com.itszuvalex.itszulib.gui

/**
  * Created by Christopher Harris (Itszuvalex) on 9/3/15.
  */
class GuiSpacingPanel(override var anchorX: Int,
                      override var anchorY: Int,
                      override var _panelWidth: Int,
                      override var _panelHeight: Int) extends GuiPanel


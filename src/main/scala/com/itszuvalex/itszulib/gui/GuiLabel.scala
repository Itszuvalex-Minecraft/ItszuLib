package com.itszuvalex.itszulib.gui

import com.itszuvalex.itszulib.util.Color
import net.minecraft.client.Minecraft
import org.lwjgl.opengl.GL11

/**
  * Created by Christopher Harris (Itszuvalex) on 9/17/15.
  */
object GuiLabel {
  val DEFAULT_FONT_COLOR: Int = Color(255.toByte, 255.toByte, 255.toByte, 255.toByte).toInt
}

class GuiLabel(override var anchorX: Int,
               override var anchorY: Int,
               override var _panelWidth: Int,
               override var _panelHeight: Int,
               var text: () => String = () => "",
               var scaling: Float = 1f) extends GuiPanel {
  var colorFont: Int = GuiLabel.DEFAULT_FONT_COLOR
  var xPadding       = 0
  var yPadding       = 0

  override def render(screenX: Int, screenY: Int, mouseX: Int, mouseY: Int, partialTicks: Float): Unit = {
    super.render(screenX, screenY, mouseX, mouseY, partialTicks)
    GL11.glPushMatrix()
    GL11.glScalef(scaling, scaling, scaling)
    GL11.glEnable(GL11.GL_BLEND)
    Minecraft.getMinecraft.fontRenderer.drawSplitString(text(), screenX + xPadding, screenY + yPadding, panelWidth - 2 * xPadding, colorFont)
    GL11.glPopMatrix()
  }
}

/*
 * ******************************************************************************
 *  * Copyright (C) 2013  Christopher Harris (Itszuvalex)
 *  * Itszuvalex@gmail.com
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *  *****************************************************************************
 */
package com.itszuvalex.itszulib.util

import java.io.File

import net.minecraft.client.Minecraft
import net.minecraft.world.World
import net.minecraftforge.fml.common.FMLCommonHandler
import net.minecraftforge.fml.relauncher.Side


/**
  * Created by Christopher Harris (Itszuvalex) on 7/26/14.
  */
object FileUtils {
  def savePathMod(world: World, modID: String): String = {
    val dir = new File(savePath(world), modID.toLowerCase)
    if (!dir.exists) {
      dir.mkdir
    }
    dir.getPath
  }

  def savePath(world: World): String = world.getSaveHandler.getWorldDirectory.getAbsolutePath

  def configFolder(modID: String): File = {
    val path = configPath(modID)
    val f    = new File(path)
    if (!f.exists()) {
      f.mkdir()
    }
    f
  }

  def customConfigPath(modID: String): String = configPath(modID) + "custom" + File.separator

  def configPath(modID: String): String = if (FMLCommonHandler.instance.getEffectiveSide == Side.SERVER) {
    FMLCommonHandler.instance.getMinecraftServerInstance.getFile(FMLCommonHandler.instance().getMinecraftServerInstance.getFolderName + File.separator + "config" + File.separator + modID + File.separator).getPath + File.separator
  } else {Minecraft.getMinecraft.mcDataDir + File.separator + "config" + File.separator + modID + File.separator}

  def autogenConfigPath(modID: String): String = configPath(modID) + "autogen" + File.separator
}




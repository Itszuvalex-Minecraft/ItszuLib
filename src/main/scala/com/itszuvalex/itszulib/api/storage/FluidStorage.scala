package com.itszuvalex.itszulib.api.storage

import net.minecraftforge.fluids.FluidTank

class FluidStorage(capacity: Int) extends FluidStorageTank(new FluidTank(capacity))

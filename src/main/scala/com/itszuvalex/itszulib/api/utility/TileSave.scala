package com.itszuvalex.itszulib.api.utility

import com.itszuvalex.itszulib.ItszuLib
import com.itszuvalex.itszulib.api.core.{BlockIdentifier, Loc4}
import com.itszuvalex.itszulib.api.wrappers.IWorld
import net.minecraft.block.Block
import net.minecraft.block.state.IBlockState
import net.minecraft.nbt.NBTTagCompound
import net.minecraft.tileentity.TileEntity
import net.minecraft.util.math.BlockPos
import net.minecraftforge.common.util.INBTSerializable

object TileSave {
  def apply(nBTTagCompound: NBTTagCompound): TileSave = loadFromNBT(nBTTagCompound)


  def loadFromNBT(compound: NBTTagCompound): TileSave = {
    new TileSave(compound.getInteger("dimension"),
                 compound.getInteger("posX"),
                 compound.getInteger("posY"),
                 compound.getInteger("posZ"),
                 BlockIdentifier(compound.getCompoundTag("bi")),
                 compound.getInteger("meta"),
                 if (compound.hasKey("nbt")) compound.getCompoundTag("nbt") else null)
  }
}

class TileSave(private var _dimensionID: Int, var pos: BlockPos, var bi: BlockIdentifier, var meta: Int, var te: NBTTagCompound) extends INBTSerializable[NBTTagCompound] {

  lazy val block: Block = bi.block

  def this(dim: Int, x: Int, y: Int, z: Int, bi: BlockIdentifier, meta: Int, nbt: NBTTagCompound) =
    this(dim, new BlockPos(x, y, z), bi, meta, nbt)

  def this(dim: Int, x: Int, y: Int, z: Int, bi: BlockIdentifier, state: IBlockState, nbt: NBTTagCompound) =
    this(dim, x, y, z, bi, state.getBlock.getMetaFromState(state), nbt)

  def this(dim: Int, pos: BlockPos, bi: BlockIdentifier, state: IBlockState, nbt: NBTTagCompound) =
    this(dim, pos, bi, state.getBlock.getMetaFromState(state), nbt)

  def world: IWorld = ItszuLib.proxy.getIWorld(dimensionID)

  def world_=(world: IWorld): Unit = _dimensionID = world.dimensionId

  def this(dimensionID: Int, pos: BlockPos, state: IBlockState, te: NBTTagCompound) =
    this(dimensionID,
         pos,
         BlockIdentifier(state.getBlock),
         state,
         te)

  def this(world: IWorld, pos: BlockPos, te: NBTTagCompound) =
    this(world.dimensionId,
         pos,
         world.getBlockState(pos),
         te)

  def this(dimensionID: Int, pos: BlockPos, state: IBlockState, te: TileEntity) =
    this(dimensionID, pos, state, if (te != null) {
      val nbt = new NBTTagCompound
      te.writeToNBT(nbt)
      nbt
    } else {
      null
    })

  def this(world: IWorld, pos: BlockPos, state: IBlockState, te: TileEntity) =
    this(world, pos, if (te != null) {
      val nbt = new NBTTagCompound
      te.writeToNBT(nbt)
      nbt
    } else {
      null
    })

  def this(world: IWorld, pos: BlockPos) =
    this(world, pos, world.getBlockState(pos), world.getTileEntity(pos))

  def this(loc: Loc4) = this(loc.getWorld.get, loc.getPos)

  override def serializeNBT(): NBTTagCompound = {
    val compound = new NBTTagCompound
    compound.setInteger("dimension", dimensionID)
    compound.setInteger("posX", pos.getX)
    compound.setInteger("posY", pos.getY)
    compound.setInteger("posZ", pos.getZ)
    compound.setTag("bi", bi.serializeNBT())
    compound.setInteger("meta", meta)
    if (te != null) compound.setTag("nbt", te)
    compound
  }

  def dimensionID: Int = _dimensionID

  def dimensionID_=(dim: Int): Unit = _dimensionID = dim

  override def deserializeNBT(compound: NBTTagCompound): Unit = {
    _dimensionID = compound.getInteger("dimension")
    pos = new BlockPos(compound.getInteger("posX"),
                       compound.getInteger("posY"),
                       compound.getInteger("posZ"))
    bi = BlockIdentifier(compound.getCompoundTag("bi"))
    meta = compound.getInteger("meta")
    te = if (compound.hasKey("nbt")) compound.getCompoundTag("nbt") else null
  }
}
